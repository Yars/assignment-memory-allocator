#include "mem.h"
#include <stdio.h>

void test1() {
    // Normal use case of _malloc and _free
    int *a = _malloc(sizeof(int));
    int *b = _malloc(sizeof(int));
    *a = -123;
    *b = 3432;
    printf("%d\n%d\n", *a, *b);
    printf("Test 1 passed!\n");
    _free(a);
    _free(b);
}

void test2() {
    // Freeing one of the blocks
    int *a = _malloc(sizeof(int));
    int *b = _malloc(sizeof(int));
    *a = -123;
    *b = 3432;
    printf("%d\n%d\n", *a, *b);
    _free(a);   // Freeing a
    printf("Test 2 passed!\n");
    _free(b);
}

void test3() {
    // Freeing two of the blocks
    int *a = _malloc(sizeof(int));
    long long *b = _malloc(sizeof(long long));
    int *c = _malloc(sizeof(int));
    *a = -123;
    *b = 31302587432;
    *c = 124124;
    printf("%d\n%lld\n%d\n", *a, *b, *c);
    _free(a);   // Freeing a
    _free(c);   // Freeing b
    printf("Test 3 passed!\n");
    _free(b);
}

void test4() {
    // New region allocation
    int *a = _malloc(sizeof(int) * 64); // The array is bigger than 32 bytes (initial size of the heap)
    for (size_t i = 0; i < 64; i++) {
        a[i] = (int)(i * i * i);
        printf("%d ", a[i]);
    }
    printf("\n");
    _free(a);
    printf("Test 4 passed!\n");
}

void test5() {
    // New region allocation somewhere else
    int *a = _malloc(sizeof(int) * 8);  // 32 bytes
    int *b = _malloc(sizeof(int) * 32); // Growing the heap with a new region
    for (size_t i = 0; i < 32; i++) {
        b[i] = (int)(i * i * i);
        printf("%d ", b[i]);
    }
    printf("\n");
    _free(a);   // Freeing the first array without freeing the second one
    a = (int*) _malloc(sizeof(int) * 128);  // Trying to allocate 512 bytes in a space which only has 32 bytes, a new region will be allocated somewhere else
    for (size_t i = 0; i < 128; i++) {
        a[i] = (int)(i * i);
        printf("%d ", a[i]);
    }
    printf("\n");
    _free(b);
    _free(a);
    printf("Test 5 passed!\n");
}

int main(int argc, char** argv) {
    if (argc > 1) {
        if (((char)argv[1][0] - '0') < 1 || ((char)argv[1][0] - '0') > 5) {
            printf("Invalid argument format.\n   Run all tests: main\n  Run particular test: main [1..5]\n");
        } else {
            heap_init(32);
            switch ((char) (argv[1][0] - '0')) {
                case 1:
                    test1();
                    break;
                case 2:
                    test2();
                    break;
                case 3:
                    test3();
                    break;
                case 4:
                    test4();
                    break;
                case 5:
                    test5();
                    break;
            }
        }
    } else {
        heap_init(32);
        test1();
        test2();
        test3();
        test4();
        test5();
    }
	return 0;
}
